# Laravel Log Processor

## objective

Add processor and config for logs.

## Get started

### Install

```bash
composer config repositories.gitlab.com/pecqueurs-public composer https://gitlab.com/api/v4/group/53853709/-/packages/composer/

composer require pecqueurs/laravel-log-processor
```

### Publish config

One shot :
```bash
php artisan vendor:publish --provider='PecqueurS\LaravelLogProcessor\Providers\LaravelLogProcessorProvider' --tag='config' --ansi
```

in composer.json for each update :

```json
...
"scripts": {
    ...
    "post-update-cmd": [
        ...
        "@php artisan vendor:publish --provider='PecqueurS\\LaravelLogProcessor\\Providers\\LaravelLogProcessorProvider' --tag='config' --ansi"
    ],
    ...
},
...
```

### Usage

#### Add config

example config in `./config/log_processor.php`: 

```php
return [
    'docker' => [
        'driver' => 'single',
        'path' => storage_path('logs/laravel.log'),
        'level' => env('LOG_LEVEL', 'debug'),
        'formatter' => Monolog\Formatter\JsonFormatter::class,
        'tap' => [
            App\Services\Logs\LogWebProcessor::class,
            App\Services\Logs\LogUidProcessor::class,
            App\Services\Logs\LogMemoryUsageProcessor::class,
            App\Services\Logs\LogHostnameProcessor::class,
            App\Services\Logs\LogGitCommitProcessor::class
        ],
    ],
];
```


example config in `./config/logging.php`:

```php
return [

    ...

    'channels' => [...[
        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'replace_placeholders' => true,
        ],

        ...
    ], ...PecqueurS\LaravelLogProcessor\LogConfig::get()],

    ...

];
```
