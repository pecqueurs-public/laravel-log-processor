<?php

namespace PecqueurS\LaravelLogProcessor;

use Illuminate\Support\ServiceProvider;

class LogConfig
{
    public static function get()
    {
        return config('log_processor');
    } 
}