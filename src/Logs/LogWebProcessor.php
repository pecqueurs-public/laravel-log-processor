<?php

namespace PecqueurS\LaravelLogProcessor\Logs;

use PecqueurS\LaravelLogProcessor\Logs\Processors\WebProcessorExtended;
use Illuminate\Log\Logger;
use Monolog\Processor\WebProcessor;

class LogWebProcessor
{
    /**
     * Customize the given logger instance.
     *
     * @param Logger $logger
     * @return void
     */
    public function __invoke(Logger $logger)
    {
        collect($logger->getHandlers())->each(function ($handler) {
            if (!app()->runningInConsole()) {
                $handler->pushProcessor(new WebProcessor(extraFields: [
                    'url' => 'REQUEST_URI',
                    'server' => 'SERVER_NAME',
                    'referrer' => 'HTTP_REFERER',
                    "request_time" => "REQUEST_TIME_FLOAT",
                    "http_host" => 'HTTP_HOST'
                ]));
                $handler->pushProcessor(new WebProcessorExtended());
            }
        });
    }
}
