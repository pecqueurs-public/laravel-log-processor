<?php

namespace PecqueurS\LaravelLogProcessor\Logs;

use Monolog\Processor\HostnameProcessor;

class LogHostnameProcessor
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        collect($logger->getHandlers())->each(function ($handler) {
            $handler->pushProcessor(new HostnameProcessor());
        });
    }

}
