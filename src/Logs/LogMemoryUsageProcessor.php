<?php

namespace PecqueurS\LaravelLogProcessor\Logs;

use Illuminate\Log\Logger;
use Monolog\Processor\MemoryUsageProcessor;

class LogMemoryUsageProcessor
{
    /**
     * Customize the given logger instance.
     *
     * @param Logger $logger
     * @return void
     */
    public function __invoke(Logger $logger)
    {
        collect($logger->getHandlers())->each(function ($handler) {
            $handler->pushProcessor( new MemoryUsageProcessor(true, false));
        });
    }
}
