<?php

namespace PecqueurS\LaravelLogProcessor\Logs;

use Illuminate\Log\Logger;
use PecqueurS\LaravelLogProcessor\Logs\Processors\UidProcessor;

class LogUidProcessor
{
    /**
     * Customize the given logger instance.
     *
     * @param Logger $logger
     * @return void
     */
    public function __invoke(Logger $logger)
    {
        collect($logger->getHandlers())->each(function ($handler) {
            $handler->pushProcessor(new UidProcessor);
        });
    }
}
