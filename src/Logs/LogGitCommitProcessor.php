<?php

namespace PecqueurS\LaravelLogProcessor\Logs;

use PecqueurS\LaravelLogProcessor\Logs\Processors\GitCommitProcessor;

class LogGitCommitProcessor
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        collect($logger->getHandlers())->each(function ($handler) {
            $handler->pushProcessor(new GitCommitProcessor());
        });
    }

}
