<?php declare(strict_types=1);

namespace PecqueurS\LaravelLogProcessor\Logs\Processors;

use Monolog\Processor\ProcessorInterface;
use Monolog\ResettableInterface;
use Illuminate\Support\Str;
use Monolog\LogRecord;

/**
 * Adds a unique identifier into records
 *
 */
class UidProcessor implements ProcessorInterface, ResettableInterface
{
    /** @var string */
    private $uid;

    public function __construct()
    {
        $this->uid = Str::uuid();
    }

    /**
     * {@inheritDoc}
     */
    public function __invoke(LogRecord $record)
    {
        $record['extra']['uid'] = $this->uid;

        return $record;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function reset(): void
    {
        $this->uid = Str::uuid();
    }
}
