<?php declare(strict_types=1);

namespace PecqueurS\LaravelLogProcessor\Logs\Processors;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;

/**
 * Injects value of gethostname in all records
 */
class GitCommitProcessor implements ProcessorInterface
{

    public function __construct()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function __invoke(LogRecord $record)
    {
        if(empty($record['extra']['github']['commit']) && env('GITHUB_SHA') !== null) {
            $record['extra']['github']['commit'] = env('GITHUB_SHA');
        }

        return $record;
    }
}
