<?php declare(strict_types=1);

namespace PecqueurS\LaravelLogProcessor\Logs\Processors;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;

class SlackProcessor  implements ProcessorInterface
{

/**
 * @param array $record
 * @return array
 */
public function __invoke(LogRecord $record)
{
    $record["context"]["Env"] = app()->environment();

    return $record;
}
}
