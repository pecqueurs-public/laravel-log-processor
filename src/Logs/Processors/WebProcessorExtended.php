<?php declare(strict_types=1);

namespace PecqueurS\LaravelLogProcessor\Logs\Processors;

use Monolog\Processor\ProcessorInterface;
use Illuminate\Support\Str;
use Monolog\LogRecord;

/**
 * Injects url/method and remote IP of the current web request in all records
 *
 */
class WebProcessorExtended implements ProcessorInterface
{
    public function __construct()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function __invoke(LogRecord $record)
    {
        $record['extra']['ip'] = request()->ip();
        $record['extra']['device_os'] = (string) Str::of(request()->header('Sec-CH-UA-Platform'))->trim('"');
        $record['extra']['http_method'] = request()->method();
        $record['extra']['user_agent'] = request()->header('User-Agent');

        return $record;
    }
}
