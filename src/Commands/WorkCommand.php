<?php

namespace PecqueurS\LaravelLogProcessor\Commands;

use Carbon\CarbonInterval;
use Illuminate\Queue\Console\WorkCommand as BaseWorkCommand;
use Illuminate\Contracts\Queue\Job;
use ReflectionClass;

class WorkCommand extends BaseWorkCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'horizon:work
                            {connection? : The name of the queue connection to work}
                            {--name=default : The name of the worker}
                            {--delay=0 : The number of seconds to delay failed jobs (Deprecated)}
                            {--backoff=0 : The number of seconds to wait before retrying a job that encountered an uncaught exception}
                            {--max-jobs=0 : The number of jobs to process before stopping}
                            {--max-time=0 : The maximum number of seconds the worker should run}
                            {--daemon : Run the worker in daemon mode (Deprecated)}
                            {--force : Force the worker to run even in maintenance mode}
                            {--memory=128 : The memory limit in megabytes}
                            {--once : Only process the next job on the queue}
                            {--stop-when-empty : Stop when the queue is empty}
                            {--queue= : The names of the queues to work}
                            {--sleep=3 : Number of seconds to sleep when no job is available}
                            {--rest=0 : Number of seconds to rest between jobs}
                            {--supervisor= : The name of the supervisor the worker belongs to}
                            {--timeout=60 : The number of seconds a child process can run}
                            {--tries=0 : Number of times to attempt a job before logging it failed}';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (config('horizon.fast_termination')) {
            ignore_user_abort(true);
        }

        parent::handle();
    }

    /**
     * Write the status output for the queue worker.
     *
     * @param  \Illuminate\Contracts\Queue\Job  $job
     * @param  string  $status
     * @return void
     */
    protected function writeOutput(Job $job, $status)
    {
        $serialized = $job->payload()['data']['command'] ?? '';

        $data['context'] = [
            'date' => $this->now()->format('Y-m-d H:i:s'),
            'job' => $job->resolveName(),
            'job_id' => $job->getJobId(),
            'queue' => $job->getQueue(),
            'connection' => $job->getConnectionName(),
            'payload' => $this->getUnserializedPayload($serialized),
        ];

        if ($status == 'starting') {
            $this->latestStartedAt = microtime(true);
            $data['context']['status'] = 'RUNNING';
            $data['message'] = "{$data['context']['job']} ({$data['context']['job_id']}) {$data['context']['status']}";

            return $this->output->writeln(json_encode($data));
        }

        $data['context']['time'] = $this->formatRunTime($this->latestStartedAt);
        $data['context']['status'] = match ($status) {
            'success' => 'DONE',
            default => 'FAIL',
        };
        $data['message'] = "{$data['context']['job']} ({$data['context']['job_id']}) {$data['context']['status']} ({$data['context']['time']})";

        $this->output->writeln(json_encode($data));
    }

    protected function getUnserializedPayload(string $serialized): array
    {
        $object = unserialize($serialized);
        $reflectionClass = new ReflectionClass($object);
        $properties = $reflectionClass->getProperties();
        $extractedData = [];
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $propName = $property->getName();
            $cleanName = preg_replace('/^\0.*\0/', '', $propName);
            if (preg_match('/\d+:"[^"]*' . $cleanName . '"/', $serialized)) {
                $extractedData[$cleanName] = $property->getValue($object);
            }
        }

        return $extractedData;
    }

    /**
     * Given a start time, format the total run time for human readability.
     *
     * @param  float  $startTime
     * @return string
     */
    protected function formatRunTime($startTime)
    {
        $runTime = (microtime(true) - $startTime) * 1000;

        return $runTime > 1000
            ? CarbonInterval::milliseconds($runTime)->cascade()->forHumans(short: true)
            : number_format($runTime, 2).'ms';
    }
}
