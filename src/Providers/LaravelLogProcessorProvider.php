<?php

namespace PecqueurS\LaravelLogProcessor\Providers;

use Illuminate\Support\ServiceProvider;

class LaravelLogProcessorProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/log_processor.php' => config_path('log_processor.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../Config/log_processor.php', 'log_processor'
        );
    }
}

