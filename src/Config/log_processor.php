<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

// define channels type to use on app or not and slack notification if APP_DEVOPS mode is engaged

$channel = [];
if (env('APP_ENV') === 'testing') {
    $channels[] = 'test';
} else {
    if (env('APP_ENV') === 'local') {
        $channels[] = 'debug';
    }

    $channels[] = 'docker';

    if (env('APP_DEVOPS')) {
        $channels[] = 'slack';
    }
}

return [
    'stack' => [
        'driver' => 'stack',
        'channels' => $channels,
        'ignore_exceptions' => false,
    ],

    'slack' => [
        'driver' => 'slack',
        'url' => env('SLACK_WEBHOOK'),
        'username' => null,
        'emoji' => null,
        'level' => env('SLACK_LOG_LEVEL', 'critical'),
        'replace_placeholders' => true,
        'tap' => [
            //PecqueurS\LaravelLogProcessor\Logs\SlackEnvProcessor::class,
        ],
    ],

    'debug' => [
        'driver'  => 'monolog',
        'handler' => \Monolog\Handler\FilterHandler::class,
        'handler_with' => [
            'handler' => new \Monolog\Handler\StreamHandler(storage_path('logs/debug.log')),
            'minLevelOrList' => [\Monolog\Level::Debug->value],
        ],
        'formatter' => Monolog\Formatter\JsonFormatter::class,
        'tap' => [
            PecqueurS\LaravelLogProcessor\Logs\LogWebProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogUidProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogMemoryUsageProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogHostnameProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogGitCommitProcessor::class
        ],
    ],

    'docker' => [
        'driver' => 'single',
        'path' => storage_path('logs/laravel.log'),
        'level' => env('LOG_LEVEL', 'debug'),
        'formatter' => Monolog\Formatter\JsonFormatter::class,
        'tap' => [
            PecqueurS\LaravelLogProcessor\Logs\LogWebProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogUidProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogMemoryUsageProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogHostnameProcessor::class,
            PecqueurS\LaravelLogProcessor\Logs\LogGitCommitProcessor::class
        ],
    ],

    'test' => [
        'driver' => 'single',
        'path' => storage_path('logs/test.log'),
        'level' => env('LOG_LEVEL', 'debug'),
        'replace_placeholders' => true,
    ],
];
